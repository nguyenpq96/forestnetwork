import {take, put, fork} from 'redux-saga/effects';
import {delay} from 'redux-saga';
import requestAction from '../actionType/request';

function* resetRequestSuccessStateFlow() {
    while(true) {
        yield take(requestAction.REQUEST_SUCCESS);
        yield delay(2000);
        yield put({type: requestAction.REQUEST_RESET_STATE});
    }
}

function* resetRequestFailedStateFlow() {
    while(true) {
        yield take(requestAction.REQUEST_ERROR);
        yield delay(2000);
        yield put({type: requestAction.REQUEST_RESET_STATE});
    }
}

export default function* requestFlow() {
    yield fork(resetRequestSuccessStateFlow);
    yield fork(resetRequestFailedStateFlow)
}