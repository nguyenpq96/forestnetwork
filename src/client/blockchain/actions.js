import Transaction from 'src/client/blockchain/Transaction.js';
import { getStatus } from 'src/api/api';
import { get as getBlock } from 'src/models/blocks';
import { searchByAddress as searchTransactionsByAddress } from 'src/models/transactions';
import { decode } from 'forest.network/lib/tx';
import Promise from 'bluebird';
import moment from 'moment';
import { createHash } from 'crypto';
import axios from 'axios';

const LIMIT = 100;
const TRE_TO_CEL = 100000000;
const BANDWIDTH_PERIOD = 86400;
const MAX_BLOCK_SIZE = 22020096;
const RESERVE_RATIO = 1;
const MAX_CELLULOSE = Number.MAX_SAFE_INTEGER;
const NETWORK_BANDWIDTH = RESERVE_RATIO * MAX_BLOCK_SIZE * BANDWIDTH_PERIOD;

const transferMoney = async (sender, reception, amount, privateKey) => {
  const hashedTx = await Transaction.payment(sender, reception, amount, privateKey);
  let response = await axios.get(`https://dragonfly.forest.network/broadcast_tx_commit?tx=0x${hashedTx}`);

  return response;
};

const createAccount = async (publicKey, privateKey, address) => {
  const hashedTx = await Transaction.createAccount(publicKey, privateKey, address);
  let response = await axios.get(`https://dragonfly.forest.network/broadcast_tx_commit?tx=0x${hashedTx}`);
 
  return response;
};

const createInteract =  async (publicKey, privateKey, params) => {
  const hashedTx = await Transaction.createInteract(publicKey, privateKey, params);
  let response = await axios.get(`https://dragonfly.forest.network/broadcast_tx_commit?tx=0x${hashedTx}`);
 
  return response;
};


const updateAccount = async (publicKey, privateKey, params) => {
    const hashedTx = await Transaction.updateAccount(publicKey, privateKey, params);
    let response = await axios.post('https://dragonfly.forest.network/', {
        "method": 'broadcast_tx_commit',
        "jsonrpc": "2.0",
        "params": {tx: `${hashedTx}`},
        "id": "anything"
    }, {
        headers: { 'Content-Type': 'text/plain' }
    });

    return response;
};

const createPost = async (publicKey, privateKey, params) => {
    const hashedTx = await Transaction.createNewPost(publicKey, privateKey, params);
    let response = await axios.post('https://dragonfly.forest.network/', {
        "method": 'broadcast_tx_commit',
        "jsonrpc": "2.0",
        "params": {tx: `${hashedTx}`},
        "id": "anything"
    }, {
        headers: { 'Content-Type': 'text/plain' }
    });

    return response;
};

const calculateBalance = async (publicKey) => {
    const transactions = await Transaction.getTransactions(publicKey);
    const paymentTransactions = Transaction.getTransactionsWithType(transactions, Transaction.TransactionType.PAYMENT);
    let spent, leftover;
    spent = leftover = 0;

    for (let transaction of paymentTransactions) {
        const { address, amount } = transaction.params;

        if (address !== publicKey) {
            spent += amount;
        } else {
            leftover += amount;
        }
    }

    return { spent, leftover };
};

const getSequenceByAddress = async (address) => {
    let sequence = 0;
    const records = [];
    let transactions = [];
    const preFlight = await searchTransactionsByAddress(address, 1, 1);
    const total = preFlight.total_count;
    if (total > 0) {
        let page = 1;
        while (records.length < total) {
            const nextTxs = await searchTransactionsByAddress(address, LIMIT, page);
            records.push(...nextTxs.txs);
            page += 1;
        }
        transactions = records.map(({ tx, height }) => {
            const data = Buffer.from(tx, 'base64');
            return {
                ...decode(data),
                height,
                size: data.length,
                hash: createHash('sha256').update(data).digest().toString('hex').toUpperCase(),
            };
        });
        transactions.forEach(tx => {
            if (tx.account === address) {
                sequence = tx.sequence;
            }
        });
    }

    return sequence;
}

const getProfileByAddress = async (address) => {
    const records = [];
    let name = '';
    let picture = '';
    let transactions = [];
    let balance = 0;
    let energy = 0;
    let sequence = 0;
    let bandwidth = 0;
    // Get first transaction for count tx
    const preFlight = await searchTransactionsByAddress(address, 1, 1);
    const total = preFlight.total_count;
    if (total > 0) {
        let page = 1;
        while (records.length < total) {
            const nextTxs = await searchTransactionsByAddress(address, LIMIT, page);
            records.push(...nextTxs.txs);
            page += 1;
        }
        transactions = records.map(({ tx, height }) => {
        const data = Buffer.from(tx, 'base64');
        return {
            ...decode(data),
            height,
            size: data.length,
            hash: createHash('sha256').update(data).digest().toString('hex').toUpperCase(),
        };
        });
        // Check genesis account
        balance = transactions[0].account === address
            ? MAX_CELLULOSE
            : 0;
        transactions.forEach(tx => {
            if (tx.account === address) {
                sequence = tx.sequence;
                if (tx.operation === 'update_account') {
                    const { key, value } = tx.params;
                    if (key === 'name') {
                        name = value.toString('utf-8');
                    } else if (key === 'picture' && value.length > 0) {
                        picture = `data:image/jpeg;base64,${value.toString('base64')}`;
                    }
                }
            }
            if (tx.operation === 'payment') {
                if (tx.params.address === address) {
                    balance += tx.params.amount;
                } else {
                    balance -= tx.params.amount;
                }
            }
        });
        balance = balance / TRE_TO_CEL;
        // Caculate energy
        let bandwidthTime = null;
        const bandwidthLimit = Math.floor(balance * TRE_TO_CEL / MAX_CELLULOSE * NETWORK_BANDWIDTH);
        const status = await getStatus();
        await Promise.each(transactions.concat({
        // Imagine last tx
            account: address,
            size: 0,
            height: status.sync_info.latest_block_height,
        }), async (tx) => {
            const block = await getBlock(tx.height, true);
            tx.time = block.block_meta.header.time;
            if (tx.account === address) {
                const time = moment(tx.time).unix();
                const diff = bandwidthTime
                ? time - bandwidthTime
                : BANDWIDTH_PERIOD;
                bandwidth = Math.ceil(Math.max(0, (BANDWIDTH_PERIOD - diff) / BANDWIDTH_PERIOD) * bandwidth + tx.size);
                bandwidthTime = time;
                energy = bandwidthLimit - bandwidth;
            }
        });
    }

    return {
        name,
        picture,
        balance,
        energy,
        sequence,
        transactions,
    }
}


export default {
    transferMoney,
    createAccount,
    calculateBalance,
    createInteract,
    getProfileByAddress,
    updateAccount,
    getSequenceByAddress,
    createPost
};
