import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import PersonalTweet from "src/containers/PersonalTweet/PersonalTweet";

class TweetList extends Component {
    render() {
        const {posts} = this.props;
        return (
            <Fragment>
                {posts.reverse().map((post, index) => (
                    <PersonalTweet key={index} post={post} />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = () => ({});

export default connect(mapStateToProps, {})(TweetList);