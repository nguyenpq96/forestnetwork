import actionTypes from '../actionType/auth';

const initialState = {
    privateKey: '',
    publicKey: '',
}

const auth = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.AUTH_LOGIN_SUCCESS:
            const { privateKey, publicKey } = action.payload;
            return {
                ...state,
                privateKey,
                publicKey,
            };
        case actionTypes.AUTH_LOGOUT_SUCCESS:
            return {
                ...state,
                privateKey: '',
                publicKey: '',
            };
        default:
            return state;
    }
};

export default auth;