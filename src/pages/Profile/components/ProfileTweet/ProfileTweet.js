import React, { Component } from 'react';
import TimelineTweet from "src/containers/TimelineTweet/TimelineTweet";
import { get as getAccount } from 'src/models/accounts';
import {sendRequest, requestSuccess, requestFailed} from "src/redux/actions/request";
import { connect } from 'react-redux';
import NewFeed from 'src/components/NewFeed/NewFeed';
import LoadingFeed from 'src/components/LoadingFeed/LoadingFeed';
import Promise from 'bluebird';
import Waypoint from 'react-waypoint';

class ProfileTweet extends Component {
    constructor(props) {
        super(props);

        this.state = {
            newfeeds: [],
            pageSize: 10,
            pages: 0,
            loading: false,
        }
    }

    componentDidMount = async () => {
        this.setState({ loading: true });
        await this.getNewFeeds();
        this.setState({ loading: false });
    }
    

    componentDidUpdate = async (prevProps) => {
        if(prevProps.profile.feeds !== this.props.profile.feeds) {
            this.setState({ loading: true });
            await this.getNewFeeds();
            this.setState({ loading: false });
        }
    }
    

    getNewFeeds = async () => {
        let newfeeds = [];
        const { feeds } = this.props.profile;
        newfeeds = [].concat(feeds);
        await Promise.each(newfeeds, async (feed) => {
            feed.fromAccount = await getAccount(feed.account, true);
            if (feed.params.address) {
                feed.toAccount = await getAccount(feed.params.address, true);
            }
            if (feed.followings) {
                feed.followings = await Promise.mapSeries(feed.followings, async (account) => {
                    return getAccount(account.address, true);
                });
            }
        });
        newfeeds = newfeeds.sort((a, b) => {
            if (a.time > b.time) return -1;
            if (a.time < b.time) return 1;
            return 0;
        });
        this.setState({ newfeeds, pages: newfeeds.length / 10 });
    }

    handleLoadMore = () => {
        const { pageSize } = this.state;
        this.props.sendRequest();
        setTimeout(() => {
            this.setState({ pageSize: pageSize + 10 });
            this.props.requestSuccess();
        }, 300);
    }

    _renderWaypoint = (page) => {
        const { pages } = this.state;
        if(page < pages) {
            return <Waypoint onEnter={this.handleLoadMore} bottomOffset={-162} />;
        }
        return null;
    }

    render() {
        const { newfeeds, pageSize, loading } = this.state;
        return (
            <React.Fragment>
                <TimelineTweet />
                {loading && (
                    <LoadingFeed />
                )}
                {newfeeds.slice(0, pageSize).map((feed, index) => (
                    <NewFeed feed={feed} key={index} />
                ))}
                {this._renderWaypoint(pageSize / 10)}
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    profile: state.profile,
});

export default connect(mapStateToProps, {
    sendRequest,
    requestSuccess,
    requestFailed,
})(ProfileTweet);