import axios from '../axios-instance/request';

const cached = {
    blocks: {},
    transactions: {},
};

async function request(path, params = {}) {
    const { data, error } = await axios.get(path, {
        params
    });
    if (error) {
        throw Error(error);
    }
    return data.result;
}

export async function getBlock(height) {
    if (!cached.blocks[height.toString()]) {
        cached.blocks[height.toString()] = await request('block', { height });
    }
    return cached.blocks[height.toString()];
}

export async function getBlocks(minHeight, maxHeight) {
    return request('blockchain', { minHeight, maxHeight });
}

export async function getStatus(url = '') {
    return request(`${url}status`);
}

export async function getNetworkInfo(url = '') {
    return request(`${url}net_info`);
}

export async function getTransaction(hash) {
    if (!cached.transactions[hash]) {
        cached.transactions[hash] = await request('tx', { hash: `0x${hash}` });
    }
    return cached.transactions[hash];
}

export async function searchTransactions(query, limit, page) {
    return request('tx_search', {
        query,
        page,
        per_page: limit,
    });
}
