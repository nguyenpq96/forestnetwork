import { get as getBlock, getChain } from './blocks';
import { get as getTransaction, hash as hashTransaction } from './transactions';
import { decode } from 'forest.network/lib/tx';

import Promise from 'bluebird';

const LONGEST_CHAIN = 20;

export async function getLatest(height, limit) {
    const feeds = [];
    let maxHeight = height;
    let minHeight;
    while (maxHeight > 1 && feeds.length < limit) {
        minHeight = maxHeight - LONGEST_CHAIN + 1 > 0 ? maxHeight - LONGEST_CHAIN + 1 : 1;
        const blocks = await getChain(minHeight, maxHeight);
        await Promise.each(blocks, async (b) => {
            if (Number(b.block_meta.header.num_txs) > 0) {
                const block = await getBlock(b.block_meta.header.height);
                await Promise.each(block.block.data.txs, async (tx) => {
                    const data = Buffer.from(tx, 'base64');
                    const hash = hashTransaction(data);
                    const transaction = await getTransaction(hash);
                    const decodedTransaction = decode(Buffer.from(transaction.tx, 'base64'));
                    // Interact is not a feed
                    if (decodedTransaction.operation === 'interact') {
                        return;
                    }
                    feeds.push({
                        tx: decodedTransaction,
                        raw: transaction,
                        block,
                    });
                });
            }
        });
        maxHeight = minHeight - 1;
    }
    return feeds;
}