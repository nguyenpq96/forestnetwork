import {take, put, fork, call} from 'redux-saga/effects';
import profileAction from '../actionType/profile';
import requestAction from '../actionType/request';
import blockchain from '../../client/blockchain/actions';

function* setupProfile() {
    while(true) {
        const { payload, callback } = yield take(profileAction.PROFILE_SETUP);
        try {
            const {publicKey} = payload;
            if(publicKey) {
                const profile = yield call(blockchain.getProfileByAddress, publicKey);
                if(profile) {
                    yield put({ type: profileAction.PROFILE_SETUP_SUCCESS, payload: profile });
                    yield put({ type: requestAction.REQUEST_SUCCESS });
                    if(callback && typeof callback === 'function') {
                        callback();
                    }
                } else {
                    yield put(requestAction.REQUEST_ERROR);
                }
            }
        } catch (error) {
            yield put(requestAction.REQUEST_ERROR);
        }
    }
}

export default function* profileFlow() {
    yield fork(setupProfile);
}