import React, { Component } from 'react';
import {Card,Avatar,Row,Col} from "antd";
import {connect} from "react-redux";
import {sendRequest, requestSuccess, requestFailed} from "src/redux/actions/request";
import Promise from 'bluebird';
import { get as getAccount } from 'src/models/accounts';
import { Link } from 'react-router-dom';


const { Meta } = Card;

class ProfileFollowing extends Component {

    constructor(props) {
        super(props);

        this.state = {
            followings: [],
        }
    }

    componentDidMount = async () => {
        this.setState({ loading: true });
        await this.getNewFeeds();
        this.setState({ loading: false });
    }
    

    componentDidUpdate = async (prevProps) => {
        if(prevProps.profile.feeds !== this.props.profile.feeds) {
            this.setState({ loading: true });
            await this.getNewFeeds();
            this.setState({ loading: false });
        }
    }

    getNewFeeds = async () => {
        let { followings } = Object.assign({}, this.props.profile);
        await Promise.each(followings, async (follow) => {
            follow.account = await getAccount(follow.address, true);
        });
        this.setState({ followings });
    }

    render() {
        const { followings } = this.state;
         const cover = <div style={{height: 100, backgroundColor: '#1DA1F2'}}>
        </div>        
        return (
            <div>
                <Row>
                    {followings.map((follow, index) => (
                         <Col span={12} key={index}>
                            <Card
                                cover={cover}
                            >
                                <Meta
                                    avatar={<Avatar src={
                                        follow.account.picture
                                        ? follow.account.picture
                                        : `https://api.adorable.io/avatars/256/${follow.address}.png`
                                    } />}
                                    title={<Link
                                        to={`/profile/${follow.address}`}
                                    >
                                        {follow.account.name ? follow.account.name : follow.address.slice(0, 10) + '...'}
                                    </Link>}
                                    description={`@${follow.account.name ? follow.account.name : follow.address.slice(0, 10) + '...'}`}
                                />
                            </Card>
                        </Col>
                    ))}
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    followings: state.profile.followings,
    profile: state.profile,
});

export default connect(mapStateToProps, {
    sendRequest,
    requestSuccess,
    requestFailed,
})(ProfileFollowing);
