import React, {Component} from "react";
import {connect} from "react-redux";
import {Card, Row, Col, Icon, Input, message} from "antd";
import moment from "moment";
import AvatarTweet from "src/components/AvatarTweet/AvatarTweet";
import Comment from "src/components/Comment/Comment";
import {Link} from 'react-router-dom';
import Promise from 'bluebird';
import { get as getAccount } from 'src/models/accounts';
import "./newFeed.css";
import vstruct from 'varstruct';
import tendermint from '../../client/blockchain/actions';
import {sendRequest} from "src/redux/actions/request";
import {setupProfile} from "src/redux/actions/profile";

const PlainTextContent = vstruct([
  { name: 'type', type: vstruct.UInt8 },
  { name: 'text', type: vstruct.VarString(vstruct.UInt16BE) },
]);

const ReactContent = vstruct([
  { name: 'type', type: vstruct.UInt8 },
  { name: 'reaction', type: vstruct.UInt8 },
]);


class NewFeed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comments: [],
            reactions: [],
            comment: '',
        }
    }

    componentDidMount = async () => {
        await this.getInteracts();
    }

    componentDidUpdate = async (prevProps) => {
        if(prevProps.profile.interacts !== this.props.profile.interacts) {
            await this.getInteracts();
        }
    }

    getInteracts = async () => {
        const { hash } = this.props.feed;
        let { interacts } = Object.assign({}, this.props.profile);
        await Promise.each(interacts, async (interact) => {
            interact.fromAccount = await getAccount(interact.account, true);
        });
        interacts = interacts.filter(interact => interact.toHash === hash);
        const comments = interacts.filter(interact => interact.comment !== null);
        const reactions = interacts.filter(interact => interact.reaction !== null);
        this.setState({ 
            comments,
            reactions,
        });
    }

    handleCommentChange = (event) => {
        this.setState({ comment: event.target.value });
    }
    
    handleEmitEmpty = () => {
        this.commentInput.focus();
        this.setState({ comment: '' });
    }

    handleSendComment = async () => {
        const publicKey = window.sessionStorage.getItem('publicKey');
        const privateKey = window.sessionStorage.getItem('privateKey');
        const { comment } = this.state;
        if(comment) {
            const { hash } = this.props.feed;
            const { energy } = this.props.profile;
            const textParams = {
                type: 1,
                text: comment,
            };

            let content = PlainTextContent.encode(textParams);
            const commentParams = {
                object:  hash,
                content: new Buffer(content),
            }
            let bufferSize = Buffer.from(JSON.stringify(commentParams));
            if(bufferSize.length < energy) {
                this.props.sendRequest();
                const commentReponse = await tendermint.createInteract(publicKey, privateKey, commentParams);
                if(commentReponse.status === 200) {
                    this.props.setupProfile({ publicKey }, () => {
                        this.setState({ comment: '' })
                    });
                } else {
                    message.error("Errorrrrr");
                }
            } else {
                message.warning('Not enough energy');
            }
        } else {
            message.error('Comment must be has value');
        }
    }

    handleReaction = async () => {
        const publicKey = window.sessionStorage.getItem('publicKey');
        const privateKey = window.sessionStorage.getItem('privateKey');
        const { hash } = this.props.feed;
        const { energy } = this.props.profile;
        const reactParams = {
            type: 2,
            reaction: 2,
        };

        let content = ReactContent.encode(reactParams);
        const commentParams = {
            object:  hash,
            content: new Buffer(content),
        }
        let bufferSize = Buffer.from(JSON.stringify(commentParams));
        if(bufferSize.length < energy) {
            this.props.sendRequest();
            const reactReponse = await tendermint.createInteract(publicKey, privateKey, commentParams);
            if(reactReponse.status === 200) {
                this.props.setupProfile({ publicKey });
            } else {
                message.error("Errorrrrr");
            }
        } else {
            message.warning('Not enough energy');
        }
    }

    render() {
        const { feed } = this.props;
        const { picture } = this.props.profile;
        const { comments, comment, reactions } = this.state;
        const suffix = comment ? <Icon type="close-circle" onClick={this.handleEmitEmpty} /> : null;
        return (
            <Card
                className="feed-item"
                loading={false}
                hoverable
                style={{marginBottom: 10}}
                bodyStyle={{paddingTop: 10, paddingBottom: 10, marginBottom: 5}}
            >
                <Row>
                    <Col span={3}>
                        <AvatarTweet 
                            size={60} 
                            src={feed.fromAccount && feed.fromAccount.picture
                                ? feed.fromAccount.picture
                                : `https://api.adorable.io/avatars/256/${feed.account}.png`} 
                        />
                    </Col>
                    <Col span={21}>
                        <div className="stream-item-header">
                            <span className="account-name">
                                <Link to={`/profile/${feed.account}`}>
                                    {feed.fromAccount && feed.fromAccount.name && feed.fromAccount.name.length > 0
                                        ? feed.fromAccount.name
                                        : feed.account.slice(0, 10) + '...'}
                                </Link>
                            </span>
                            <span className="account-screen-name">
                                @{feed.fromAccount && feed.fromAccount.name && feed.fromAccount.name.length > 0
                                    ? feed.fromAccount.name
                                    : feed.account.slice(0, 10) + '...'}</span>
                            <span className="account-seperator"> . </span>
                            <span className="account-time">{moment(feed.time).fromNow()}</span>
                        </div>
                        <div className="js-tweet-text-container">
                            {feed.operation === 'post' && feed.post && `${feed.post.join('\n')}`}
                            {feed.operation === 'create_account' && (feed.toAccount && feed.toAccount.name && feed.toAccount.name.length > 0
                                ? `Created account: ${feed.toAccount.name}` 
                                : `Created account: ${feed.account}`)}
                            {feed.operation === 'payment' && (feed.params.amount && feed.toAccount.name && feed.toAccount.name.length > 0
                               ? (<span>
                                    Send to <Link to={`/profile/${feed.toAccount.address}`}>{feed.toAccount.name}</Link> {feed.params.amount} CEL
                                </span>)
                                :  (<span>
                                    Send to <Link to={`/profile/${feed.toAccount.address}`}>{feed.toAccount.address}</Link> {feed.params.amount} CEL
                                </span>))}
                            {feed.operation === 'update_account' && feed.params.key === 'name' && <span>Updated name to <b>{feed.params.value.toString('utf-8')}</b></span>}
                            {feed.operation === 'update_account' && feed.params.key === 'followings' && feed.followings && (
                                <span>{feed.followings.length === 0 ? 'Unfollowed all friends.' : 'Followed'} {feed.followings.map((account, index) => <span key={index}><Link to={`/profile/${account.address}`}>{account.name ? account.name : account.address.slice(0,20) + '...'}</Link> ,</span>)}</span>
                            )}
                            {feed.operation === 'update_account' && feed.params.key === 'picture' && `Updated picture with ${feed.params.value.length} bytes`}
                        </div>
                        {feed.operation === 'post' && (
                            <React.Fragment>
                                <div className="stream-item-footer">
                                    <span className="action-item" onClick={this.handleReaction}>
                                        <Icon type="heart" theme="filled" style={{ color: 'red' }}/> <b>{reactions.length}</b>
                                    </span>
                                </div>
                            </React.Fragment>
                        )}
                    </Col>
                </Row>
               
                {feed.operation === 'post' && (
                    <React.Fragment>
                        <hr/>
                        <Row style={{marginTop: 10}}>
                            {comments.map((comment, index) => (
                                <Comment comment={comment} key={index} />
                            ))}
                        </Row>
                        
                        <Row style={{marginTop: 10}}>
                            <Col span={2} offset={1} style={{display: "flex", justifyContent: "flex-end", paddingRight: 10}}>
                                <AvatarTweet 
                                    size={30} 
                                    src={picture} 
                                />
                            </Col>
                            <Col span={21}>
                                <Input 
                                    placeholder="Type your comment ..."
                                    prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    value={comment}
                                    suffix={suffix}
                                    onChange={this.handleCommentChange}
                                    onPressEnter={this.handleSendComment}
                                    ref={node => this.commentInput = node}
                                />
                            </Col>
                        </Row>
                    </React.Fragment>
                )}
            </Card>
        );
    }

}

const mapStateToProps = (state) => ({
    profile: state.profile,
});

export default connect(mapStateToProps, {
    sendRequest,
    setupProfile,
})(NewFeed);