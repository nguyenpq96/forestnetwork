import actionTypes from '../actionType/auth';

export const authLogin = (payload, callback) => ({
    type: actionTypes.AUTH_LOGIN,
    payload,
    callback,
});