import React, {Component} from "react";
import {connect} from "react-redux";
import {Row, Col, Icon, Card, Input, message} from "antd";
import {Redirect, Switch} from "react-router-dom";

import Content from "src/layout/Main/components/Content/Content";
import ProfileHeader from './components/ProfileHeader/ProfileHeader';
import {RouteWithSubRoutes} from "src/routes/routeWithSubRoutes";
import {appConfigs} from "src/constants/configs";

//action creator
import {updateProfile, setupProfile} from "src/redux/actions/profile";
import {sendRequest} from "src/redux/actions/request";

//tendermint
import tendermint from 'src/client/blockchain/actions';

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function getArrayBuffer(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsArrayBuffer(img);
}

function beforeUpload(file) {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
        message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 < 20;
    if (!isLt2M) {
        message.error('Image must smaller than 20kb!');
    }
    return isJPG && isLt2M;
}

class MyProfile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editProfile: false,
            profile: props.profile,
            selectedPicture: null,
            uploading: false,
            uploadImageUrl: null,
        }
    }

    componentDidMount = async () => {
        const publicKey = window.sessionStorage.getItem('publicKey');
        if(publicKey) {
            this.props.sendRequest();
            this.props.setupProfile({publicKey});
        }
    }

    componentWillMount() {
        const publicKey = window.sessionStorage.getItem('publicKey');
        if(!publicKey) {
            this.props.history.push('/login');
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.profile !== this.props.profile) {
            this.setState({ 
                profile: this.props.profile,
            });
        }

        if(prevProps.profile.transactions !== this.props.profile.transactions) {
            const {transactions} = this.props.profile;
            this.props.history.push({
                pathname: '/my-profile/post',
                state: {
                    transactions,
                }
            });
        }
    }

    handleProfileChange = (event) => {
        const {name, value} = event.target;
        let newProfile = {...this.state.profile};
        newProfile[name] = value;

        this.setState({ 
            profile: newProfile
        });
    }

    handleEditProfile = () => {
        this.setState({editProfile: true});
    }

    handleSaveProfile = async () => {
        const {name} = this.state.profile;
        const {selectedPicture} = this.state;
        const { energy } = this.props.profile;
        const publicKey = window.sessionStorage.getItem('publicKey');
        const privateKey = window.sessionStorage.getItem('privateKey');
        const updateNameParams = {
            key: 'name',
            value: new Buffer(name),
        }

        let updatePictureParams = {};
        if(selectedPicture) {
            updatePictureParams.key = 'picture';
            updatePictureParams.value = new Buffer(selectedPicture);
        }
        let bufferNameSize = Buffer.from(JSON.stringify(updateNameParams));
        let pictureSize = selectedPicture ? selectedPicture.length : 0;
        if(pictureSize + bufferNameSize.length < energy) {
            this.props.sendRequest();
            let updateName = null;
            let updatePicture = null;
            if(this.props.profile.name !== name) {
                updateName = await tendermint.updateAccount(publicKey, privateKey, updateNameParams);
                if(updateName.status !== 200) message.error('Update name error');
                else message.success('Update name success');
            }
            if(selectedPicture) {
                updatePicture = await tendermint.updateAccount(publicKey, privateKey, updatePictureParams);
                if(updatePicture.status !== 200) message.error('Update picture error');
                else message.success('Update picture success');
            }

            this.props.setupProfile({publicKey}, () => {
                this.setState({ editProfile: false });
            });
        } else {
            message.warning("Not enough energy");
        }
    }
    
    handleCancelEdit = () => {
        const {profile} = this.props;
        this.setState({
            profile,
            editProfile: false,
        });
    }

    handleUploadAvatar = (event) => {
        this.setState({ uploading: true });
        const img = event.target.files[0];
        const imageIsGood =  beforeUpload(img);
        if(imageIsGood) {
            getBase64(img, uploadImageUrl => {
                this.setState({
                    uploadImageUrl,
                });
            })
            getArrayBuffer(img, arrayBuffer => {
                this.setState({
                    selectedPicture: new Uint8Array(arrayBuffer),
                    uploading: false,
                });
            });
        }
    }

    render() {
        const {editProfile, profile, uploadImageUrl} = this.state;
        const {match, routes} = this.props;
        return (
            <div>
                <Row style={{ height: 200, background: appConfigs.colors.primary}} />
                <ProfileHeader
                    transactions={profile.transactions}
                    editing={editProfile}
                    onEditProfile={this.handleEditProfile}
                    onSaveProfile={this.handleSaveProfile}
                    onCancelEdit={this.handleCancelEdit}
                    match={match}
                    isMyProfile={true}
                    avatar={editProfile ? (uploadImageUrl ? uploadImageUrl : profile.picture ) : profile.picture}
                />
                <Content style={{marginTop: 5}}>
                    <Row>
                        <Col span={6}>
                            {!editProfile && (
                                <div style={{padding:"20px 0 0 30px",fontWeight:"400",fontSize:15}}>
                                    <h2>{profile.name ? profile.name : "Account"}</h2>
                                    <p>@{profile.name ? profile.name : "Account"}</p>
                                    <p><Icon type="wallet" /> {profile.energy} OXY</p>
                                    <p><Icon type="wallet" /> {profile.balance} TRE</p>
                                </div>
                            )}
                            {editProfile && (
                                <Card 
                                    style={{margin:"20px 10px 0 30px", backgroundColor: appConfigs.colors.light}}
                                    bodyStyle={{padding: 12}}
                                >
                                    <Input 
                                        size="large" 
                                        name="name"
                                        placeholder="Account Name"
                                        value={profile.name}
                                        prefix={<Icon type="twitter" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        onChange={this.handleProfileChange}
                                    />
                                    <p style={{marginBottom: 5, marginTop: 5, fontSize: 15}}>Upload Avatar <Icon type="picture" /></p>
                                    <input type="file" onChange={this.handleUploadAvatar} />
                                </Card>
                            )}
                        </Col>
                        <Col span={12}>
                            <Switch>
                                {routes.map((route, i) => (
                                    <RouteWithSubRoutes key={i} {...route} />
                                ))}
                                <Redirect from={`/my-profile`} to={`/my-profile/post`} /> 
                            </Switch>
                        </Col>
                        <Col span={6}></Col>
                    </Row>
                </Content>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    profile: state.profile,
    auth: state.auth,
});

export default connect(mapStateToProps, {
    updateProfile, 
    setupProfile,
    sendRequest
})(MyProfile);