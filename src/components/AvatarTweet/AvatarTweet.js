import React, {Component} from "react";
import PropTypes from "prop-types";
import {Avatar} from "antd";

import "./avatarTweet.css";

class AvatarTweet extends Component {
    static propTypes = {
        size: PropTypes.number,
        src: PropTypes.string
    };

    static defaultProps = {
        size: 55
    };

    render() {
        const {src, size} = this.props;
        if (src) {
            return <Avatar className="avatar" size={size} {...this.props} src={src}/>;
        } else {
            return <Avatar size={size} {...this.props}src="https://assets.materialup.com/uploads/4483b3bb-2960-4ec7-baa7-8be0ff52bebc/avatar.png"/>;
        }
    }
}

export default AvatarTweet;