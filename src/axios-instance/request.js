import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: 'https://dragonfly.forest.network'
});

export default axiosInstance;