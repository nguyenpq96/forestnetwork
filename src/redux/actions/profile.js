import actionTypes from '../actionType/profile';

export const updateProfile = (payload) => ({
    type: actionTypes.UPDATE_PROFILE,
    payload,
});

export const setupProfile = (payload, callback) => ({
    type: actionTypes.PROFILE_SETUP,
    payload,
    callback,
});

export const setupProfileSuccess = (payload) => ({
    type: actionTypes.PROFILE_SETUP_SUCCESS,
    payload,
})