import React, { Component } from 'react';
import {Col, Layout, Menu, Row, Icon} from "antd";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {profileMenuItems} from "src/constants/menuItems";
import AvatarTweet from "src/components/AvatarTweet/AvatarTweet";
import TweetButton from 'src/components/TweetButton/TweetButton';

const {Header,Content} = Layout;

class ProfileHeader extends Component {
    render() {
        const {editing, match, avatar, transactions, isMyProfile} = this.props;
        const menuItems = profileMenuItems.filter((menuItem) => isMyProfile ? isMyProfile : (menuItem.both && !isMyProfile));
        const { followings } = this.props.profile;
        const { profileId } = match.params;
        let followed = false;
        followings.forEach(follow => {
            if(follow.address === profileId) {
                followed = true;
            }
        });
        return (
            <Header style={{width: '100%', backgroundColor: "#FFF", height: 60}}>
                <Content style={{padding:"0 60px"}}>
                    <Row>
                        <Col span={6}>
                            <AvatarTweet size={220} 
                                src={avatar}
                                style={{
                                    position: "absolute",
                                    top: -140,
                                    left: 20,
                                    zIndex: 1,
                                    border: "4px solid #fff"
                                }} 
                            />
                        </Col>
                        <Col span={12}>
                            <Menu
                                theme="light"
                                mode="horizontal"
                                defaultSelectedKeys={['0']}
                                style={{lineHeight: '60px'}}
                            >
                                {menuItems.map((menuItem, index) => (
                                    <Menu.Item key={index}>
                                        <Link to={{
                                            pathname: `${match.url}${menuItem.path}`,
                                            state: {transactions},
                                        }}>
                                            <b>{menuItem.title}</b>
                                        </Link>
                                    </Menu.Item>
                                ))}
                            </Menu>
                        </Col>
                        <Col span={6}>
                            {isMyProfile ? (
                                <React.Fragment>
                                    {!editing && (
                                        <TweetButton size="large"
                                            clicked={this.props.onEditProfile}
                                        >
                                            Edit profile
                                        </TweetButton>
                                    )}
                                    {editing && (
                                        <React.Fragment>
                                            <TweetButton size="large"
                                                clicked={this.props.onCancelEdit}
                                            >
                                                Cancel
                                            </TweetButton>
                                            <TweetButton size="large"
                                                type="primary"
                                                clicked={this.props.onSaveProfile}
                                            >
                                                Save changes
                                            </TweetButton>
                                        </React.Fragment>
                                    )}
                                </React.Fragment>
                            ) : (
                                <TweetButton type={`${followed ? 'primary' : ''}`} size="large"
                                    clicked={() => this.props.onFollow(followed, profileId)}
                                >
                                    {followed && <Icon type="check" />}{followed ? 'Followed' : 'Follow'}
                                </TweetButton>
                            )}
                        </Col>
                    </Row>
                </Content>
            </Header>
        );
    }
}

const mapStateToProps = (state) => ({
    profile: state.profile,
});

export default connect(mapStateToProps, {
})(ProfileHeader);