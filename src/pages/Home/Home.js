import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col } from "antd";
import DashboardProfileCard from "src/containers/DashboardProfileCard/DashboardProfileCard";
import Content from "src/layout/Main/components/Content/Content";
import Promise from 'bluebird';
import vstruct from 'varstruct';
import base32 from 'base32.js';
import { get as getAccount } from '../../models/accounts';
import tendermint from '../../client/blockchain/actions';
import {setupProfileSuccess} from "src/redux/actions/profile";
import {sendRequest, requestSuccess, requestFailed} from "src/redux/actions/request";
import NewFeed from 'src/components/NewFeed/NewFeed';
import { unionBy } from 'lodash';
import Waypoint from 'react-waypoint';

const PlainTextContent = vstruct([
  { name: 'type', type: vstruct.UInt8 },
  { name: 'text', type: vstruct.VarString(vstruct.UInt16BE) },
]);

const Followings = vstruct([
  { name: 'addresses', type: vstruct.VarArray(vstruct.UInt16BE, vstruct.Buffer(35)) },
]);

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            newfeeds: [],
            pageSize: 10,
            pages: 0,
        }
    }

    componentWillMount() {
        const publicKey = window.sessionStorage.getItem('publicKey');
        if(!publicKey) {
            this.props.history.push('/login');
        }
    }

    componentDidMount = async () => {
        const publicKey = window.sessionStorage.getItem('publicKey');
        if(publicKey) {
            this.props.sendRequest();
            const profile = await tendermint.getProfileByAddress(publicKey);
            if(profile) this.props.setupProfileSuccess(profile);
            await this.getNewFeeds(publicKey);
            this.props.requestSuccess();
        }
    }
  
    getFeedsByAccount = async (address) => {
        const { transactions } = await getAccount(address, false);
        const feeds = transactions.filter(tx => tx.operation !== 'interact');
        let followings = [];
        feeds.forEach((feed) => {
            if (feed.operation === 'post') {
                try {
                    if (feed.params.keys.length > 0) {
                        throw Error('Cannot decrypt post');
                    }
                    const post = PlainTextContent.decode(feed.params.content);
                    if (post.type !== 1) {
                        throw Error('Cannot decode post');
                    }
                    feed.post = post.text.split(/\r|\n/);
                } catch (err) {
                    feed.post = null;
                }
            } else if (feed.operation === 'update_account') {
                if (feed.params.key === 'followings') {
                    try {
                        feed.followings = Followings
                            .decode(feed.params.value)
                            .addresses.map(a => ({
                                address: base32.encode(a),
                            }));
                        followings = feed.followings;
                    } catch (err) {
                        feed.followings = [];
                    }
                }
            }
        });
        return {
            followings,
            feeds,
        };
    }

    getNewFeeds = async (address) => {
        let newfeeds = [];
        const { feeds, followings } = await this.getFeedsByAccount(address);
        newfeeds = [].concat(feeds);
        await Promise.each(followings, async (account) => {
            const followingFeeds = await this.getFeedsByAccount(account.address);
            newfeeds = newfeeds.concat(followingFeeds.feeds);
        });
        await Promise.each(newfeeds, async (feed) => {
            feed.fromAccount = await getAccount(feed.account, true);
            if (feed.params.address) {
                feed.toAccount = await getAccount(feed.params.address, true);
            }
            if (feed.followings) {
                feed.followings = await Promise.mapSeries(feed.followings, async (account) => {
                    return getAccount(account.address, true);
                });
            }
        });
        newfeeds = newfeeds.sort((a, b) => {
            if (a.time > b.time) return -1;
            if (a.time < b.time) return 1;
            return 0;
        });
        newfeeds = unionBy(newfeeds, 'time');
        this.setState({ newfeeds, pages: newfeeds.length / 10 });
    }

    handleLoadMore = () => {
        const { pageSize } = this.state;
        this.props.sendRequest();
        setTimeout(() => {
            this.setState({ pageSize: pageSize + 10 });
            this.props.requestSuccess();
        }, 300);
    }

    _renderWaypoint = (page) => {
        const { pages } = this.state;
        if(page < pages) {
            return <Waypoint onEnter={this.handleLoadMore} bottomOffset={-162} />;
        }
        return null;
    }


    render() {
        const { newfeeds, pageSize } = this.state;
        return (
            <Content>
                <Row>
                    <Col span={6}>{<DashboardProfileCard />}</Col>
                    <Col span={12}>
                        {newfeeds.slice(0, pageSize).map((feed, index) => (
                            <NewFeed feed={feed} key={index} />
                        ))}
                        {this._renderWaypoint(pageSize/10)}
                    </Col>
                    <Col span={6}></Col>
                </Row>
            </Content>
        );
    }
}

const mapStateToProps = (state) => ({
    profile: state.profile
});

export default connect(mapStateToProps, {
    setupProfileSuccess,
    sendRequest,
    requestSuccess,
    requestFailed
})(Home);