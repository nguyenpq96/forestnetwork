import React, {Component} from "react";
import {Col, Layout, Menu, Row, Icon} from "antd";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {menuItems} from "src/constants/menuItems";
import AvatarTweet from "src/components/AvatarTweet/AvatarTweet";
import { withRouter } from 'react-router-dom';

import './Header.css';

const {Header} = Layout;
const {SubMenu} = Menu;
const MenuItemGroup = Menu.ItemGroup;

class BaseHeader extends Component {
    handleSignOut = () => {
        window.sessionStorage.removeItem('publicKey');
        window.sessionStorage.removeItem('privateKey');
        this.props.history.push('/login');
    }

    render() {
        const {profile} = this.props;
        return (
            <Header style={{position: 'fixed', zIndex: 2, width: '100%', backgroundColor: "#FFF", height: 46}}>
                <Row>
                    <Col span={18} offset={1}>
                        <Menu
                            theme="light"
                            mode="horizontal"
                            defaultSelectedKeys={['0']}
                            style={{lineHeight: '46px'}}
                        >
                            {menuItems.map((menuItem, index) =>
                                <Menu.Item key={index}>
                                    <Link to={menuItem.path}>
                                        <Icon type={menuItem.iconType}/> {menuItem.title}
                                    </Link>
                                </Menu.Item>
                            )}
                        </Menu>
                    </Col>
                    <Col span={4}>
                        <Menu
                            theme="light"  
                            mode="horizontal"
                            defaultSelectedKeys={['0']}
                            style={{lineHeight: '46px'}}
                        >
                            <SubMenu 
                                style={{borderBottom:0,}}
                                title={ <AvatarTweet 
                                    size={25} 
                                    src={profile.picture} 
                                />}
                            >
                                <MenuItemGroup title={<div>
                                        <b style={{fontSize:18,fontWeight:"bold",color:"#000"}}>{profile.name ? profile.name : "Account"}</b><br/>
                                        <span style={{fontSize:15,}}>@{profile.name ? profile.name : "Account"}</span>
                                    </div>}
                                >
                                    <Menu.Item key={0}>
                                        <Link to={`/my-profile`}>
                                            <Icon type="user" /> Profile
                                        </Link>
                                    </Menu.Item>
                                    <Menu.Item key={3} onClick={this.handleSignOut}>
                                        <Icon type="logout" /> Sign out
                                    </Menu.Item>
                                </MenuItemGroup>
                            </SubMenu>
                        </Menu>
                    </Col>
                </Row>
            </Header>
        );
    }
}

const mapStateToProps = (state) => ({
    profile: state.profile,
});

export default connect(mapStateToProps, {})(withRouter(BaseHeader));