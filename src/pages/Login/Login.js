import React, { Component } from 'react';
import BaseLayout from "src/components/Layout/Layout";
import LoginHeader from "./components/LoginHeader/LoginHeader";
import TweetButton from 'src/components/TweetButton/TweetButton';
import {Layout, Col, Row, Card, Icon, Input, message} from 'antd';
import {Link} from 'react-router-dom';
import {authLogin} from "src/redux/actions/auth";
import {connect} from "react-redux";
import {Keypair} from 'stellar-base';

const {Content} = Layout;

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            privateKey: '',
            register: false,
            keypair: null,
        }
    }

    handleOnUserKeyChange = (event) => {
        this.setState({privateKey: event.target.value.trim()});
    }

    handleEmitEmpty = () => {
        this.userKeyInput.focus();
        this.setState({ privateKey: '' });
    }

    handleLoginClick = () => {
        const { privateKey } = this.state;
        this.props.authLogin({privateKey}, (error) => {
            if(error) message.error('Account not exists');
            else this.props.history.push(`/home`);
        });
    }

    handleSwitchPage = (isRegister) => {
        this.setState({ register: isRegister, keypair: null });
    }

    handleRandomKeypair = () => {
        const key = Keypair.random();
        this.setState({
            keypair: {
                privateKey: key.secret(),
                publicKey: key.publicKey(),
            }
        });
    }

    render() {
        const{privateKey,register,keypair} = this.state;
        const suffix = privateKey ? <Icon type="close-circle" onClick={this.handleEmitEmpty} /> : null;

        return (
            <BaseLayout style={{backgroundColor: "#F0F2F5"}}>
               <LoginHeader menuIndex={0} />
               <Row style={{padding: '0 50px', marginTop: 60}}>
                    <Col span={16} offset={4}>
                        <Content>
                            <Card style={{
                                padding: "10px 40px"
                            }}>
                                {!register && (
                                    <React.Fragment>
                                        <h2>Log in to Forest Network</h2>
                                        <Row>
                                            <Input
                                                style={{width: 400}}
                                                placeholder="Enter your key"
                                                prefix={<Icon type="key" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                                suffix={suffix}
                                                value={privateKey}
                                                onChange={this.handleOnUserKeyChange}
                                                ref={node => this.userKeyInput = node}
                                            />
                                        </Row>
                                        <Row style={{marginTop: 5}}>
                                            <TweetButton type="primary" style={{width: 100}}
                                                clicked={this.handleLoginClick}
                                            >
                                                Log in
                                            </TweetButton>
                                        </Row>
                                        <Row>
                                            <Link to="#" onClick={() => this.handleSwitchPage(true)}>
                                                Or register an account ...
                                            </Link>
                                        </Row>
                                    </React.Fragment>
                                )}
                                {register && (
                                    <React.Fragment>
                                        <h2>Create new keypair</h2>
                                        {keypair && (
                                            <Row style={{marginTop: 10, fontSize: 15}}>
                                                <code>
                                                    Secret Key: {keypair.privateKey} <Icon type='copy' />
                                                </code> <br/>
                                                <code>
                                                    Public Key: {keypair.publicKey} <Icon type='copy' />
                                                </code>
                                            </Row>
                                        )}
                                        <Row style={{marginTop: 5}}>
                                            <TweetButton type="primary" style={{width: 400}}
                                                clicked={this.handleRandomKeypair}
                                            >
                                                Random keypair
                                            </TweetButton>
                                        </Row>
                                        {keypair && <Row style={{marginTop: 5}}>
                                            <Link to="#" onClick={() => this.handleSwitchPage(false)}>
                                                Back to login ...
                                            </Link>
                                        </Row>}
                                    </React.Fragment>
                                )}
                            </Card>
                        </Content>
                    </Col>
                </Row>
            </BaseLayout>
        );
    }
};

const mapStateToProps = (state) => ({
});

export default connect(mapStateToProps, {authLogin})(Login);