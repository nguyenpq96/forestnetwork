import React, {Component} from "react";
import {connect} from "react-redux";
import {Card, Row, Col, Icon, Input} from "antd";
import moment from "moment";
import AvatarTweet from "src/components/AvatarTweet/AvatarTweet";
// import Comment from "src/components/Comment/Comment";

import "./personalTweet.css";

class PersonalTweet extends Component {
    render() {
        const { post } = this.props;
        const { picture } = this.props.profile;
        return (
            <Card
                loading={false}
                hoverable
                style={{marginBottom: 10}}
                bodyStyle={{paddingTop: 10, paddingBottom: 10, marginBottom: 5}}
            >
                <Row>
                    <Col span={3}>
                        <AvatarTweet 
                            size={60} 
                            src={post.picture} 
                        />
                    </Col>
                    <Col span={21}>
                        <div className="stream-item-header">
                            <span className="account-name">{post.name}</span>
                            <span className="account-screen-name">@{post.name}</span>
                            <span className="account-seperator"> . </span>
                            <span className="account-time">{moment(post.time).fromNow()}</span>
                        </div>
                        <div className="js-tweet-text-container">
                            {post.text}
                        </div>
                        <div className="stream-item-footer">
                            <span className="action-item" onClick={() => console.log("[comment]")}>
                                <Icon type="message"/> <b>{0}</b>
                            </span>
                            <span className="action-item" onClick={() => console.log("[like]")}>
                                <Icon type="heart"/> <b>{0}</b>
                            </span>
                        </div>
                    </Col>
                </Row>
                <hr/>
                {/* <Row style={{marginTop: 10}}>
                    {messages.map((msg, idx) => (
                        <Comment message={msg} key={idx} />
                    ))}
                </Row> */}
                <Row style={{marginTop: 10}}>
                    <Col span={2} offset={1} style={{display: "flex", justifyContent: "flex-end", paddingRight: 10}}>
                        <AvatarTweet 
                            size={30} 
                            src={picture} 
                        />
                    </Col>
                    <Col span={21}>
                        <Input />
                    </Col>
                </Row>
            </Card>
        );
    }

}

const mapStateToProps = (state) => ({
    profile: state.profile,
});

export default connect(mapStateToProps, {})(PersonalTweet);