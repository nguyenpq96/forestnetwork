import pagePaths from "src/constants/pagePaths";
import Home from "src/pages/Home/Home";
import Profile from "src/pages/Profile/Profile";
import MyProfile from "src/pages/Profile/MyProfile";
import ProfileTweet from "src/pages/Profile/components/ProfileTweet/ProfileTweet";
import ProfileFollowing from "src/pages/Profile/components/ProfileFollowing/ProfileFollowing";
import ProfileFollower from "src/pages/Profile/components/ProfileFollower/ProfileFollower";
import ProfilePayment from "src/pages/Profile/components/ProfilePayment/ProfilePayment";
import TransferMoney from "src/pages/Profile/components/TransferMoney/TransferMoney";
import InviteAccount from "src/pages/Profile/components/InviteAccount/InviteAccount";
import Login from "src/pages/Login/Login";
import AboutUs from "../pages/AboutUs/AboutUs";

const routeMapping = [
    {
        "path": `${pagePaths.HOME}`,
        "component": Home
    },
    {
        "path": `${pagePaths.MY_PROFILE}`,
        "component": MyProfile,
        "routes": [
            {
                path: `${pagePaths.MY_PROFILE}/post`,
                component: ProfileTweet,
            },
            {
                path: `${pagePaths.MY_PROFILE}/following`,
                component: ProfileFollowing
            },
            {
                path: `${pagePaths.MY_PROFILE}/follower`,
                component: ProfileFollower
            },
            {
                path: `${pagePaths.MY_PROFILE}/payment`,
                component: ProfilePayment
            },
            {
                path: `${pagePaths.MY_PROFILE}/transfer`,
                component: TransferMoney
            },
            {
                path: `${pagePaths.MY_PROFILE}/invite`,
                component: InviteAccount
            },
        ]
    },
    {
        "path": `${pagePaths.PROFILE}/:profileId`,
        "component": Profile,
        "routes": [
            {
                path: `${pagePaths.PROFILE}/:profileId/post`,
                component: ProfileTweet
            }
        ]
    },
    {
        "path": pagePaths.LOGIN,
        "component": Login,
    },
    {
        "path": pagePaths.ABOUT_US,
        "component": AboutUs,
    },
];

export default routeMapping;
