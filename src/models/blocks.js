import { getBlock, getBlocks } from '../api/api';
import { range, omit } from 'lodash';
import {
    CACHE_PREFIX as CACHE_TRANSACTION_PREFIX,
    CACHE_TIME as CACHE_TRANSACTION_TIME
} from './transactions';
import lscache from 'lscache';
import Promise from 'bluebird';
import { createHash } from 'crypto';

// 1 month
export const CACHE_TIME = 30 * 24 * 60;
export const CACHE_PREFIX = 'b';

export async function get(height, metaOnly = false) {
    const key = `${CACHE_PREFIX}:${height.toString()}`;
    const cache = lscache.get(key);
    // Header only or full block was cached
    if (cache) {
        if (metaOnly) {
            return cache;
        }
        if (cache.block) {
            if (cache.block.data.txs) {
                const txs = cache.block.data.txs.map((hash) => lscache.get(`${CACHE_TRANSACTION_PREFIX}:${hash}`));
                const missingTxs = txs.filter(tx => !tx);
                if (missingTxs.length === 0) {
                    cache.block.data.txs = txs.map(tx => tx.tx);
                    return cache;
                }
            } else {
                return cache;
            }
        }
    }
    // Omit some big field
    const result = strip(await getBlock(height));
    // Cache txs
    let txHashes = [];
    if (result.block.data.txs) {
        txHashes = result.block.data.txs.map((tx, index) => {
            const hash = createHash('sha256')
                .update(Buffer.from(tx, 'base64'))
                .digest()
                .toString('hex')
                .toUpperCase();
            // Cache tx
            lscache.set(`${CACHE_TRANSACTION_PREFIX}:${hash}`, {
                hash,
                height: height.toString(),
                index,
                tx,
            }, CACHE_TRANSACTION_TIME);
            return hash;
        });
    }
    try {
        lscache.set(key, {
            ...result,
            block: {
                ...result.block,
                data: {
                    ...result.block.data,
                    txs: txHashes,
                }
            }
        }, CACHE_TIME);
    } catch(error) {
        throw Error('Maximum cache size');
    }
    return result;
}

export async function getChain(minHeight, maxHeight) {
    let result;
    return Promise.mapSeries(range(maxHeight, minHeight - 1, -1), async (height) => {
        const key = `${CACHE_PREFIX}:${height.toString()}`;
        const cache = lscache.get(key);
        if (cache) {
            return cache;
        }
        if (!result) {
            result = await getBlocks(minHeight, maxHeight);
        }
        const found = result.block_metas.find(block => block.header.height === height.toString());
        const block = strip({ block_meta: found });
        lscache.set(key, block, CACHE_TIME);
        return block;
    });
}

function strip(block) {
    return omit(block, [
        'block.last_commit',
        'block.evidence',
        'block.header',
        'block_meta.block_id.parts',
        'block_meta.header.version',
        'block_meta.header.chain_id',
        'block_meta.header.last_block_id',
        'block_meta.header.last_commit_hash',
        'block_meta.header.data_hash',
        'block_meta.header.validators_hash',
        'block_meta.header.next_validators_hash',
        'block_meta.header.consensus_hash',
        'block_meta.header.app_hash',
        'block_meta.header.last_results_hash',
        'block_meta.header.evidence_hash',
        'block_meta.header.proposer_address',
    ]);
}