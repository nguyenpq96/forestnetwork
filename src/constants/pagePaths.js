export default {
    MAIN_APP: "/",
    HOME: "/home",
    MY_PROFILE: '/my-profile',
    PROFILE: "/profile",
    LOGIN: "/login",
    ABOUT_US: "/about"
}
