import React, { Component } from 'react';
import {Card, Icon, Input, Row, message} from "antd";
import {connect} from "react-redux";
import TweetButton from 'src/components/TweetButton/TweetButton';
import {setupProfile} from "src/redux/actions/profile";
import {sendRequest} from "src/redux/actions/request";
//tendermint
import tendermint from 'src/client/blockchain/actions';

class InviteAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            key: '',
        }
    }

    handleNewKeyChange = (event) => {
        this.setState({key: event.target.value.trim()});
    }
    
    handleEmitEmpty = () => {
        this.userKeyInput.focus();
        this.setState({ key: '' });
    }

    handleInvite = async () => {
        const { key } = this.state;
        const publicKey = window.sessionStorage.getItem('publicKey');
        const privateKey = window.sessionStorage.getItem('privateKey');
        if(key) {
            const { energy } = this.props.profile;
            let bufferSize = Buffer.from(JSON.stringify({ key }));
            if(bufferSize.length < energy) {
                this.props.sendRequest();
                const newAccount = await tendermint.createAccount(publicKey, privateKey, key);
                if(newAccount.status === 200) {
                    this.props.setupProfile({ publicKey }, () => {
                        message.success('Invite Success');
                    });
                } else {
                    message.error("Errorrrrr");
                }
            } else {
                message.warning('Not enough energy');
            }
        } else {
            message.error('Address is required');
        }
    }

    render() {
        const {key} = this.state;
        const suffix = key ? <Icon type="close-circle" onClick={this.handleEmitEmpty} /> : null;
        return (
            <Card>
                <h3><Icon type="user-add" /> Invite New Account</h3>
                <Row>
                    <Input
                        style={{width: 400}}
                        placeholder="New account key ..."
                        prefix={<Icon type="key" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        suffix={suffix}
                        value={key}
                        onChange={this.handleNewKeyChange}
                        ref={node => this.userKeyInput = node}
                    />
                </Row>
                <Row style={{marginTop: 5}}>
                    <TweetButton type="primary" style={{width: 200}}
                        clicked={this.handleInvite}
                    >
                        Invite Account
                    </TweetButton>
                </Row>
            </Card>
        );
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    profile: state.profile,
});

export default connect(mapStateToProps, {
    setupProfile,
    sendRequest
})(InviteAccount);