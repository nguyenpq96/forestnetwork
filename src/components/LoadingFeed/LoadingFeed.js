import React, { Component } from 'react';
import {Card} from "antd";

class LoadingFeed extends Component {
    render() {
        return (
            <Card
                className="feed-item"
                loading={true}
                hoverable
                style={{marginBottom: 10}}
                bodyStyle={{paddingTop: 10, paddingBottom: 10, marginBottom: 5}}
            ></Card>
        );
    }
}

export default LoadingFeed;