import actionTypes from '../actionType/request';

export const sendRequest = (payload) => ({
    type: actionTypes.REQUEST_SENDING,
    payload,
});

export const requestSuccess = (payload) => ({
    type: actionTypes.REQUEST_SUCCESS,
});

export const requestFailed = (payload) => ({
    type: actionTypes.REQUEST_ERROR,
});