import actionTypes from '../actionType/profile';
import vstruct from 'varstruct';
import base32 from 'base32.js';

const PlainTextContent = vstruct([
  { name: 'type', type: vstruct.UInt8 },
  { name: 'text', type: vstruct.VarString(vstruct.UInt16BE) },
]);

const Followings = vstruct([
  { name: 'addresses', type: vstruct.VarArray(vstruct.UInt16BE, vstruct.Buffer(35)) },
]);

const ReactContent = vstruct([
  { name: 'type', type: vstruct.UInt8 },
  { name: 'reaction', type: vstruct.UInt8 },
]);

const initialState = {
    name: "",
    balance: 0,
    energy: 0,
    picture: "",
    sequence: 0,
    transactions: [],
    posts: [],
    followings: [],
    feeds: [],
    comments: [],
    reactions: [],
    interacts: [],
};

const profile = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.PROFILE_SETUP_SUCCESS:
            const {payload} = action;
            const {transactions} = payload;
            const feeds = transactions.filter(tx => tx.operation !== 'interact');
            const interacts = transactions.filter(tx => tx.operation === 'interact');
            let followings = [];
            feeds.forEach((feed) => {
                if (feed.operation === 'post') {
                    try {
                        if (feed.params.keys.length > 0) {
                            throw Error('Cannot decrypt post');
                        }
                        const post = PlainTextContent.decode(feed.params.content);
                        if (post.type !== 1) {
                            throw Error('Cannot decode post');
                        }
                        feed.post = post.text.split(/\r|\n/);
                    } catch (err) {
                        feed.post = null;
                    }
                } else if (feed.operation === 'update_account') {
                    if (feed.params.key === 'followings') {
                        try {
                            feed.followings = Followings
                                .decode(feed.params.value)
                                .addresses.map(a => ({
                                    address: base32.encode(a),
                                }));
                            followings = feed.followings;
                        } catch (err) {
                            feed.followings = [];
                        }
                    }
                }
            });

            interacts.forEach((interact) => {
                interact.toHash = interact.params.object;
                try {
                    const reaction = ReactContent.decode(interact.params.content);
                    if(reaction.type === 2) {
                        interact.reaction = reaction;
                    } else {
                        interact.reaction = null;
                    }
                } catch (err) {
                    interact.reaction = null;
                }

                try {
                    const comment = PlainTextContent.decode(interact.params.content);
                    if(comment.type === 1) {
                        interact.comment = comment;
                    } else {
                        interact.comment = null;
                    }
                } catch (err) {
                    interact.comment = null;
                }
            });

            const comments = interacts.filter(interact => interact.comment !== null);
            const reactions =  interacts.filter(interact => interact.reaction !== null);
            return {
                ...state,
                ...payload,
                followings,
                feeds,
                comments,
                reactions,
                interacts,
            };
        case actionTypes.UPDATE_PROFILE: 
            const {id} = action.payload;
            let entities = {...state.entities};
            entities[id] = Object.assign({}, entities[id], action.payload);
            return {
                ...state,
                entities,
            };
        default:
            return state;
    }
};

export default profile;