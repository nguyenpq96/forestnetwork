import React, {Component} from "react";
import {connect} from "react-redux";
import {Card, Row, Col, Input, message} from "antd";
import AvatarTweet from "src/components/AvatarTweet/AvatarTweet";
// import IconButton from "src/components/IconButton/IconButton";
import TweetButton from 'src/components/TweetButton/TweetButton';
import vstruct from 'varstruct';
// import {iconMenuItems} from 'src/constants/menuItems';
import {setupProfile} from "src/redux/actions/profile";
import {sendRequest} from "src/redux/actions/request";

//tendermint
import tendermint from 'src/client/blockchain/actions';

import "./timelineTweet.css";

const {TextArea} = Input;

const PlainTextContent = vstruct([
  { name: 'type', type: vstruct.UInt8 },
  { name: 'text', type: vstruct.VarString(vstruct.UInt16BE) },
]);

class TimelineTweet extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
        }
    }

    handleTextChange = (event) => {
        this.setState({ text: event.target.value });
    }

    handleSubmitPost = async () => {
        const { text } = this.state;
        const publicKey = window.sessionStorage.getItem('publicKey');
        const privateKey = window.sessionStorage.getItem('privateKey');
        if(text) {
            const content = PlainTextContent.encode({ type: 1, text });
            const { energy } = this.props.profile;
            const postParams = {
                content: new Buffer(content),
                keys: [],
            };
            let bufferSize = Buffer.from(JSON.stringify(postParams));
            this.props.sendRequest();
            if(bufferSize.length < energy) {
                const newPost = await tendermint.createPost(publicKey, privateKey, postParams);
                if(newPost.status === 200) {
                    this.props.setupProfile({ publicKey }, () => {
                        this.setState({ text: '' })
                    });
                } else {
                    message.error("Errorrrrr");
                }
            } else {
                message.warning('Not enough energy');
            }
        } else {
            message.error('Post must be has value');
        }
    }

    render() {
        const { text } = this.state;
        const { picture } = this.props.profile;
        return (
            <Card
                style={{
                    backgroundColor: "#E8F5FD",
                }}
                bodyStyle={{
                    paddingTop: 10,
                    paddingBottom: 10,
                }}
            >
                <Row>
                    <Col span={3}>
                        <AvatarTweet 
                            size={35}
                            src={picture}
                        />
                    </Col>
                    <Col span={21}>
                        <TextArea style={{resize: "none"}}
                            className="tweet-input"
                            placeholder="What's happening?" 
                            rows={2}
                            value={text}
                            onChange={this.handleTextChange}
                        />
                    </Col>
                </Row>
                <Row style={{
                    paddingTop: 10,
                }}>
                    {/* <Col offset={3} span={18}>
                        {iconMenuItems.map((item, idx) => (
                            <IconButton key={idx} type={item.type} style={{
                                fontSize: 25,
                                color: "#1DA1F2",
                            }}/>
                        ))}
                    </Col> */}
                    <Col offset={21}>
                        <TweetButton type="primary"
                            clicked={this.handleSubmitPost}
                        >
                            Post
                        </TweetButton>
                    </Col>
                </Row>
            </Card>
        );
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    profile: state.profile,
});

export default connect(mapStateToProps, {
    setupProfile,
    sendRequest
})(TimelineTweet);