import React, {Component} from "react";
import {connect} from "react-redux";
import {Card, Avatar, Icon} from "antd";
import { Link } from 'react-router-dom';
const { Meta } = Card;

class DashboardProfileCard extends Component {
    render() {
        const cover = <div style={{height: 60, backgroundColor: '#1DA1F2'}}>
        </div>        
        const { profile } = this.props;
        return (
            <div>
                <Card
                    hoverable
                    cover={cover}
                    style={{width: 300}}
                >
                   <Meta
                        avatar={<Avatar src={profile.picture} size={75} />}
                        title={<Link to={`/my-profile`}>{profile.name ? profile.name : 'Account'}</Link>}
                        description={<div>
                            <span>@ {profile.name ? profile.name : 'account'}</span> <br/>
                            <span><Icon type='wallet' /> {profile.balance} TRE</span> <br/>
                            <span><Icon type='rocket' /> {profile.energy} OXY</span>
                        </div>}
                    />
                </Card>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    profile: state.profile,
});

export default connect(mapStateToProps, {})(DashboardProfileCard);