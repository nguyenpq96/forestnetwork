import { getTransaction, searchTransactions } from '../api/api';
import lscache from 'lscache';
import { createHash } from 'crypto';

// 1 month
export const CACHE_TIME = 30 * 24 * 60;
export const CACHE_PREFIX = 't';
// 1 week
export const CACHE_SEARCH_TIME = 7 * 24 * 60;
export const CACHE_SEARCH_PREFIX = 'ts';

export async function get(hash, withoutResult = false) {
    const key = `${CACHE_PREFIX}:${hash}`;
    const cache = lscache.get(key);
    // Header only or full block was cached
    if (cache && (withoutResult || cache.tx_result)) {
        return cache;
    }
    const result = await getTransaction(hash);
    lscache.set(key, result, CACHE_TIME);
    return result;
}

export async function search(query, limit, page) {
    const key = `${CACHE_SEARCH_PREFIX}:${query}:${limit}:${page}`;
    const cache = lscache.get(key);
    if (cache) {
        const txs = cache.map(hash => lscache.get(`${CACHE_PREFIX}:${hash}`));
        const missingTxs = txs.filter(tx => !tx);
        // Not missing any tx
        if (missingTxs.length === 0) {
            return {
                txs,
            };
        }
    }
    const result = await searchTransactions(query, limit, page);
    // Only cache full page
    if (limit !== 1 && result.txs.length === limit) {
        // Link only by hash
        lscache.set(key, result.txs.map(tx => tx.hash), CACHE_SEARCH_TIME);
    }
    // Try to cache partial transaction (without result)
    try {
        result.txs.forEach((tx) => {
            const txKey = `${CACHE_PREFIX}:${tx.hash}`;
            const found = lscache.get(txKey);
            if (!found || !found.tx_result) {
                lscache.set(txKey, tx, CACHE_TIME);
            }
        });
    } catch (error) {
        throw Error('Max size');
    }
    return result;
}

export async function searchByAddress(address, limit, page) {
    return search(`"account='${address}'"`, limit, page);
}

export function hash(data) {
    return createHash('sha256')
        .update(Buffer.from(data, 'base64'))
        .digest()
        .toString('hex')
        .toUpperCase();
}

