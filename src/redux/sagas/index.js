import {fork} from 'redux-saga/effects';
import accountFlow from './account';
import authFlow from './auth';
import requestFlow from './request';
import profileFlow from './profile';

console.log("Hello, redux-saga")

export default function* root() {
    yield fork(accountFlow);
    yield fork(authFlow);
    yield fork(requestFlow);
    yield fork(profileFlow);
}