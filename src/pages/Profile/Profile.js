import React, {Component} from "react";
import {connect} from "react-redux";
import {Row, Col, message} from "antd";
import Content from "src/layout/Main/components/Content/Content";
import ProfileHeader from './components/ProfileHeader/ProfileHeader';
import {appConfigs} from "src/constants/configs";
import {sendRequest, requestSuccess, requestFailed} from "src/redux/actions/request";
import {setupProfile, setupProfileSuccess} from "src/redux/actions/profile";
import NewFeed from 'src/components/NewFeed/NewFeed';
import { get as getAccount } from '../../models/accounts';
import tendermint from '../../client/blockchain/actions';
import Promise from 'bluebird';
import vstruct from 'varstruct';
import base32 from 'base32.js';
import Waypoint from 'react-waypoint';

const PlainTextContent = vstruct([
  { name: 'type', type: vstruct.UInt8 },
  { name: 'text', type: vstruct.VarString(vstruct.UInt16BE) },
]);

const Followings = vstruct([
  { name: 'addresses', type: vstruct.VarArray(vstruct.UInt16BE, vstruct.Buffer(35)) },
]);


class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            profile: {
                name: "",
                balance: 0,
                energy: 0,
                picture: "",
                sequence: 0,
                transactions: [],
            },
            newfeeds: [],
            pageSize: 10,
            pages: 0,
        }
    }

    componentWillMount() {
        const publicKey = window.sessionStorage.getItem('publicKey');
        const { profileId } = this.props.match.params;
        if(!publicKey) {
            this.props.history.push('/login');
        }
        if(publicKey === profileId) {
            this.props.history.push('/my-profile');
        }
    }

    componentDidMount = async () => {
        this.props.sendRequest();
        const publicKey = window.sessionStorage.getItem('publicKey');
        const { profileId } = this.props.match.params;
        const auth = this.props.profile;
        if(profileId) {
            const profile = await tendermint.getProfileByAddress(profileId);
            if(auth.transactions.length === 0) {
                const myProfile = await tendermint.getProfileByAddress(publicKey);
                if(myProfile) this.props.setupProfileSuccess(myProfile);
            }
            this.setState({ profile: Object.assign({}, this.state.profile, profile )});
            await this.getNewFeeds(profileId);
            this.props.requestSuccess();
        }
    }

    componentDidUpdate = async (prevProps) => {
        if(prevProps.match.params.profileId !== this.props.match.params.profileId && this.props.match.params.profileId) {
            this.props.sendRequest();
            const publicKey = window.sessionStorage.getItem('publicKey');
            const { profileId } = this.props.match.params;
            const auth = this.props.profile;
            if(profileId) {
                const profile = await tendermint.getProfileByAddress(profileId);
                if(auth.transactions.length === 0) {
                    const myProfile = await tendermint.getProfileByAddress(publicKey);
                    if(myProfile) this.props.setupProfileSuccess(myProfile);
                }
                this.setState({ profile: Object.assign({}, this.state.profile, profile )});
                await this.getNewFeeds(profileId);
                this.props.requestSuccess();
            }
        }
    }
  
    getFeedsByAccount = async (address) => {
        const { transactions } = await getAccount(address, true);
        const feeds = transactions.filter(tx => tx.operation !== 'interact');
        let followings = [];
        feeds.forEach((feed) => {
            if (feed.operation === 'post') {
                try {
                    if (feed.params.keys.length > 0) {
                        throw Error('Cannot decrypt post');
                    }
                    const post = PlainTextContent.decode(feed.params.content);
                    if (post.type !== 1) {
                        throw Error('Cannot decode post');
                    }
                    feed.post = post.text.split(/\r|\n/);
                } catch (err) {
                    feed.post = null;
                }
            } else if (feed.operation === 'update_account') {
                if (feed.params.key === 'followings') {
                    try {
                        feed.followings = Followings
                            .decode(feed.params.value)
                            .addresses.map(a => ({
                                address: base32.encode(a),
                            }));
                        followings = feed.followings;
                    } catch (err) {
                        feed.followings = [];
                    }
                }
            }
        });
        return {
            followings,
            feeds,
        };
    }

    getNewFeeds = async (address) => {
        let newfeeds = [];
        const { feeds } = await this.getFeedsByAccount(address);
        newfeeds = [].concat(feeds);
        await Promise.each(newfeeds, async (feed) => {
            feed.fromAccount = await getAccount(feed.account, true);
            if (feed.params.address) {
                feed.toAccount = await getAccount(feed.params.address, true);
            }
            if (feed.followings) {
                feed.followings = await Promise.mapSeries(feed.followings, async (account) => {
                    return getAccount(account.address, true);
                });
            }
        });
        newfeeds = newfeeds.sort((a, b) => {
            if (a.time > b.time) return -1;
            if (a.time < b.time) return 1;
            return 0;
        });
        this.setState({ newfeeds, pages: newfeeds.length / 10 });
    }

    handleFollowProfile = async (followed, address) => {
        const { energy } = this.props.profile;
        let { followings } = Object.assign({}, this.props.profile);
        followings = followings.map(follow => follow.address);
        if(followed) {
            followings = followings.filter(follow => follow !== address);
        } else {
            followings.push(address);
        }
        const followEncoded = followings.map(address => Buffer.from(base32.decode(address)));
        const arrayAddress = {
            addresses: followEncoded
        }
        let value = Followings.encode(arrayAddress);
        const followingParams = {
            key: 'followings',
            value: value,
        };
        const publicKey = window.sessionStorage.getItem('publicKey');
        const privateKey = window.sessionStorage.getItem('privateKey');
        
        let bufferSize = Buffer.from(JSON.stringify(followingParams));

        if(bufferSize.length < energy) {
            this.props.sendRequest();
            const follow = await tendermint.updateAccount(publicKey, privateKey, followingParams);
            if(follow.status === 200) {
                this.props.setupProfile({ publicKey }, () => {
                    message.success(`${followed ? 'Unfollow' : 'Follow'} Success`);
                });
            } else {
                message.error("Errorrrrr");
            }
        } else {
            message.warning('Not enough energy');
        }
    } 

    handleLoadMore = () => {
        const { pageSize } = this.state;
        this.props.sendRequest();
        setTimeout(() => {
            this.setState({ pageSize: pageSize + 10 });
            this.props.requestSuccess();
        }, 300);
    }

    _renderWaypoint = (page) => {
        const { pages } = this.state;
        if(page < pages) {
            return <Waypoint onEnter={this.handleLoadMore} bottomOffset={-162} />;
        }
        return null;
    }


    render() {
        const {editProfile, newfeeds, pageSize, profile} = this.state;
        const { match } = this.props;
        const { profileId } = match.params;
        return (
            <div>
                <Row style={{ height: 200, background: appConfigs.colors.primary}} />
                <ProfileHeader
                    editing={editProfile}
                    onEditProfile={this.handleEditProfile}
                    onSaveProfile={this.handleSaveProfile}
                    onCancelEdit={this.handleCancelEdit}
                    match={match}
                    isMyProfile={false}
                    onFollow={this.handleFollowProfile}
                    avatar={profile.picture
                            ? profile.picture
                            : `https://api.adorable.io/avatars/256/${profileId}.png`} 
                />
                <Content style={{marginTop: 5}}>
                    <Row>
                        <Col span={6}>
                            <div style={{padding:"20px 0 0 30px",fontWeight:"400",fontSize:15}}>
                                <h2>{profile.name ? profile.name : "Account"}</h2>
                                <p>@{profile.name ? profile.name : "account"}</p>
                            </div>
                        </Col>
                        <Col span={12}>
                            {newfeeds.slice(0, pageSize).map((feed, index) => (
                                <NewFeed feed={feed} key={index} />
                            ))}
                            {this._renderWaypoint(pageSize/10)}
                        </Col>
                        <Col span={6}></Col>
                    </Row>
                </Content>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    followings: state.profile.followings,
    profile: state.profile,
});

export default connect(mapStateToProps, {
    sendRequest,
    requestSuccess,
    requestFailed,
    setupProfileSuccess,
    setupProfile
})(Profile);