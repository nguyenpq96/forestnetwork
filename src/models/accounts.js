import { getStatus } from '../api/api';
import { hash } from '../models/transactions';
import { get as getBlock } from '../models/blocks';
import { searchByAddress as searchTransactionsByAddress } from '../models/transactions';
import { decode } from 'forest.network/lib/tx';
import imageType from 'image-type';
import Promise from 'bluebird';
import moment from 'moment';

// Number of transaction per page
const LIMIT = 100;

// Memory cache account by address
// Because it should be reference in many media object
const cache = {};

export const MAX_CELLULOSE = Number.MAX_SAFE_INTEGER;
export const TRE_TO_CEL = 100000000;
export const BANDWIDTH_PERIOD = 86400;
export const MAX_BLOCK_SIZE = 22020096;
export const RESERVE_RATIO = 1;
export const NETWORK_BANDWIDTH = RESERVE_RATIO * MAX_BLOCK_SIZE * BANDWIDTH_PERIOD;

export async function get(address, useCache = false) {
    if (useCache && cache[address]) {
        return cache[address];
    }
    const preFlight = await searchTransactionsByAddress(address, 1, 1);
    const total = preFlight.total_count;
    if (total === 0) {
        return null;
    }
    if (cache[address] && cache[address].transactions && cache[address].transactions.length === total) {
        return cache[address];
    }
    const account = cache[address] = cache[address] || { address };

    // Try to get all transactions
    const records = [];
    let page = 1;
    while (records.length < total) {
        const nextTxs = await searchTransactionsByAddress(address, LIMIT, page);
        records.push(...nextTxs.txs);
        page += 1;
    }
    account.transactions = records.map(({ tx, height }) => {
        const data = Buffer.from(tx, 'base64');
        return {
            ...decode(data),
            height,
            size: data.length,
            hash: hash(data),
        };
    });
    // Check genesis account
    account.balance = account.transactions[0].account === address
        ? MAX_CELLULOSE
        : 0;
    account.sequence = 0;
    account.transactions.forEach(async (tx) => {
        const block = await getBlock(tx.height, true);
        tx.time = block.block_meta.header.time;
        if (tx.account === address) {
            account.sequence = tx.sequence;
            if (tx.operation === 'update_account') {
                const { key, value } = tx.params;
                if (key === 'name') {
                    account.name = value.toString('utf-8');
                } else if (key === 'picture' && value.length > 0) {
                    // Detect image type
                    const type = imageType(value);
                    account.picture = type
                        ? `data:${type.mime};base64,${value.toString('base64')}`
                        : null;
                }
            }
        }
        if (tx.operation === 'payment') {
            if (tx.params.address === account.address) {
                account.balance += tx.params.amount;
            } else {
                account.balance -= tx.params.amount;
            }
        }
    });
    return account;
}

export async function calculateEnergy(account) {
    // Caculate energy
    let energy = 0;
    let bandwidth = 0;
    let bandwidthTime = null;
    const bandwidthLimit = Math.floor(account.balance / MAX_CELLULOSE * NETWORK_BANDWIDTH);
    const status = await getStatus();
    await Promise.each(account.transactions.concat({
        // Imagine last tx
        account: account.address,
        size: 0,
        height: status.sync_info.latest_block_height,
    }), async (tx) => {
        const block = await getBlock(tx.height, true);
        tx.time = block.block_meta.header.time;
        if (tx.account === account.address) {
            const time = moment(tx.time).unix();
            const diff = bandwidthTime
                ? time - bandwidthTime
                : BANDWIDTH_PERIOD;
            bandwidth = Math.ceil(Math.max(0, (BANDWIDTH_PERIOD - diff) / BANDWIDTH_PERIOD) * bandwidth + tx.size);
            bandwidthTime = time;
            energy = bandwidthLimit - bandwidth;
        }
    });
    return energy;
}