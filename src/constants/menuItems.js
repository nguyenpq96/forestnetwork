export const menuItems = [
    {
        title: "Home",
        path: "/home",
        iconType: "home"
    },
    {
        title: "Notifications",
        path: "/home",
        iconType: "bell"
    },
    {
        title: "Messages",
        path: "/home",
        iconType: "message"
    }
];

export const iconMenuItems = [
    {
        type: "picture"
    },
    {
        type: "paper-clip"
    },
    {
        type: "bar-chart"
    },
    {
        type: "environment"
    }
];

export const rightMenuItems = [
     {
        title: "Profile",
        path: "/profile",
        iconType: "user"
    },
    {
        title: "Log out",
        path: "/logout",
        iconType: "logout"
    }
];

export const loginMenuItems = [
    {
        title: "Home",
        path: "/login",
        iconType: "yuque"
    },
    {
        title: "About us",
        path: "/about",
    }
];

export const profileMenuItems = [
    {
        title: "Posts",
        path: "/post",
        both: true,
    },
    {
        title: "Following",
        path: "/following",
        both: false,
    },
    {
        title: "Payment History",
        path: "/payment",
        both: false,
    },
    {
        title: "Transfer Money",
        path: "/transfer",
        both: false,
    },
    {
        title: "Invite",
        path: "/invite",
        both: false,
    }
];