import {take, put, fork, call} from 'redux-saga/effects';
import authAction from '../actionType/auth';
import requestAction from '../actionType/request';
import { searchByAddress as searchTransactionsByAddress } from 'src/models/transactions';
import {Keypair} from 'stellar-base';

function* loginFlow() {
    while(true) {
        const {payload, callback} = yield take(authAction.AUTH_LOGIN);
        try {
            const {privateKey} = payload;
            let publicKey = '';
            try {
                publicKey = Keypair.fromSecret(privateKey).publicKey();
            } catch(error) {
                throw Error('Cant decode private key');
            }

            if(publicKey) {
                yield put({type: requestAction.REQUEST_SENDING});
                const response = yield call(searchTransactionsByAddress, publicKey, 1, 1);
                if(response) {
                    yield put({type: requestAction.REQUEST_SUCCESS});
                    yield put({type: authAction.AUTH_LOGIN_SUCCESS, payload: {privateKey, publicKey}});
                    window.sessionStorage.setItem('publicKey', publicKey);
                    window.sessionStorage.setItem('privateKey', privateKey);
                    if(callback && typeof callback === 'function') {
                        callback();
                    }
                } else {
                    yield put(requestAction.REQUEST_ERROR);
                }
            }
        } catch (error) {
            if(callback && typeof callback === 'function') {
                callback(error);
            }
            yield put(requestAction.REQUEST_ERROR);
        }
    }
}

export default function* authFlow() {
    yield fork(loginFlow);
}