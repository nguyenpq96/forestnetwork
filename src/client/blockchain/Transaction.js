import transaction from 'forest.network/lib/tx';
import blockchain from './actions';

const defaultVersion = 1;

const TransactionType = {
    PAYMENT: 'payment',
    POST: 'post',
    UPDATE_ACCOUNT: 'update_account',
    CREATE_ACCOUNT: 'create_account',
    INTERACT: 'interact',
};

const getBaseTx = async (account, operation) => {
    const sequence = await blockchain.getSequenceByAddress(account);
    return {
        operation,
        account,
        version: defaultVersion,
        sequence: sequence + 1,
        memo: Buffer.alloc(0)
    };
};

const payment = async (sender, reception, amount, privateKey) => {
    const baseTx = await getBaseTx(sender, TransactionType.PAYMENT);
    const crawTx = {
        ...baseTx,
        params: {
            address: reception,
            amount: amount
        }
    };
    transaction.sign(crawTx, privateKey);

    return transaction.encode(crawTx).toString('hex');
};

const createInteract = async (publicKey, privateKey, params) => {
    const baseTx = await getBaseTx(publicKey, TransactionType.INTERACT);
    const crawTx = {
        ...baseTx,
        params: {
            object: params.object,
            content: params.content,
        }
    };
    transaction.sign(crawTx, privateKey);

    return transaction.encode(crawTx).toString('hex');
};

const createAccount = async (publicKey, privateKey, newAccount) => {
    const baseTx = await getBaseTx(publicKey, TransactionType.CREATE_ACCOUNT);
    const crawTx = {
        ...baseTx,
        params: {
            address: newAccount
        }
    };
    transaction.sign(crawTx, privateKey);

    return transaction.encode(crawTx).toString('hex');
};

const updateAccount = async (publicKey, privateKey, data) => {
    const baseTx = await getBaseTx(publicKey, TransactionType.UPDATE_ACCOUNT);
    const crawTx = {
        ...baseTx,
        params: {
          key: data.key,
          value: data.value,
        }
    };

    transaction.sign(crawTx, privateKey);
    return transaction.encode(crawTx).toString('base64');
};

const createNewPost = async (publicKey, privateKey, post) => {
    const baseTx = await getBaseTx(publicKey, TransactionType.POST);
    const crawTx = {
        ...baseTx,
        params: {
          content: post.content,
          keys: post.keys, 
        }
    };
    transaction.sign(crawTx, privateKey);
    return transaction.encode(crawTx).toString('base64');
};

const decode = (data) => {
    data = new Buffer(data, 'base64');
    return transaction.decode(data);
};

const getTransactionsWithType = (transactions, type) => {
    return transactions.filter(transaction => transaction.operation === type);
};

const getTransactions = async (publicKey) => {
    const response = await fetch(`https://dragonfly.forest.network/tx_search?query="account='${publicKey}'"`);
    const json = await response.json();

    return json.result.txs.map(txs => decode(txs.tx));
};

const getSequenceTx = async (publicKey) => {
    let transactions = await getTransactions(publicKey);
    const paymentTransactions = getTransactionsWithType(transactions, TransactionType.PAYMENT);
    transactions = paymentTransactions.filter(transaction => transaction.params.address !== publicKey);

    return transactions.length ? transactions[transactions.length - 1].sequence : 0;
};

export default {
    payment,
    createAccount,
    updateAccount,
    createNewPost,
    createInteract,
    decode,
    TransactionType,
    getTransactions,
    getSequenceTx,
    getTransactionsWithType
};