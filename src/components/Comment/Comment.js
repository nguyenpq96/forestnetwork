import React, { Component } from 'react';
import moment from 'moment';
import {Avatar,Col} from "antd";
import { Link } from 'react-router-dom';
import "./comment.css";

class Comment extends Component {
    render() {
        const {comment} = this.props;
        return (
            <Col span={23} offset={1} style={{marginBottom: 10}}>
                <Col span={2} style={{display: "flex", justifyContent: "flex-end", paddingRight: 10}}>
                    <Avatar 
                        size={30} 
                        src={
                            comment.fromAccount && comment.fromAccount.picture 
                                ? comment.fromAccount.picture
                                : `https://api.adorable.io/avatars/256/${comment.account}.png`}
                    />
                </Col>
                <Col span={22}>
                    <div className="stream-item-header" style={{zIndex: 5}}>
                        <span className="account-name">
                            <Link to={`/profile/${comment.account}`}>
                                {comment.fromAccount && comment.fromAccount.name && comment.fromAccount.name.length > 0
                                    ? comment.fromAccount.name
                                    : comment.account.slice(0, 15) + '...'}
                            </Link>
                        </span>
                        <span className="account-screen-name">
                            @ {comment.fromAccount && comment.fromAccount.name && comment.fromAccount.name.length > 0
                                ? comment.fromAccount.name
                                : comment.account.slice(0, 15) + '...'}
                        </span>
                        <span className="account-seperator"> . </span>
                        <span className="account-time">{moment(comment.time).fromNow()}</span>
                    </div>
                    <div className="js-comment-text-container">
                        {comment.comment.text}
                    </div>
                </Col>
            </Col>
        );
    }
}

export default Comment;