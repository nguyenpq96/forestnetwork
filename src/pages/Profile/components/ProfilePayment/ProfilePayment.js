import React, { Component } from 'react';
import {Card, Icon, Table} from "antd";
import {connect} from "react-redux";
import { Link } from 'react-router-dom';

const columns = [{
    title: 'State',
    dataIndex: 'state',
    key: 'state',
    width: '100px',
    render: state => state === 'in' ?<Icon type="arrow-down" style={{color: 'green'}} /> : <Icon type="arrow-up" style={{color: 'red'}} /> ,
}, {
    title: 'Address',
    dataIndex: 'address',
    render: address => <Link to={`/profile/${address}`}>{address}</Link>,
    key: 'address',
}, {
    title: 'Amount',
    dataIndex: 'amount',
    key: 'amount',
    width: '200px',
}];


class ProfilePayment extends Component {
    render() {
        const {transactions} = this.props.location.state;
        const publicKey = window.sessionStorage.getItem('publicKey');
        const payments = transactions.filter(tx => tx.operation === 'payment').map((tx, index) => {
            const paymentTx = {};
            const {address, amount} = tx.params;
            paymentTx.address = address !== publicKey ? address : tx.account;
            paymentTx.amount = amount;
            paymentTx.state = address !== publicKey ? 'out' : 'in';
            paymentTx.key = index;
            return paymentTx;
        });
        return (
            <Card>
                <h3><Icon type="solution" /> Payment History</h3>
                <Table 
                    columns={columns} 
                    bordered dataSource={payments}
                    pagination={{
                        pageSize: 10,
                        total: payments.length,
                    }}
                />
            </Card>
        );
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth,
});

export default connect(mapStateToProps, {
})(ProfilePayment);