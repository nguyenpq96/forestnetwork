import React, { Component } from 'react';
import {Card, Icon, Row, Input, InputNumber, message} from "antd";
import {connect} from "react-redux";
import TweetButton from 'src/components/TweetButton/TweetButton';
import {setupProfile} from "src/redux/actions/profile";
import {sendRequest} from "src/redux/actions/request";
import tendermint from 'src/client/blockchain/actions';

class TransferMoney extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reception: '',
            amount: 1,
        }
    }

    handleReceptionChange = (event) => {
        this.setState({reception: event.target.value.trim()});
    }
    
    handleEmitEmptyReception = () => {
        this.addressInput.focus();
        this.setState({ reception: '' });
    }

    handleAmountChange = (amount) => {
        this.setState({amount});
    }
    
    handleTransfer = async () => {
        const {reception, amount} = this.state;
        const publicKey = window.sessionStorage.getItem('publicKey');
        const privateKey = window.sessionStorage.getItem('privateKey');
        if(reception) {
            let bufferSize = Buffer.from(JSON.stringify({ reception, amount }));
            const { energy } = this.props.profile;
            if(bufferSize.byteLength < energy) {
                this.props.sendRequest();
                const payment = await tendermint.transferMoney(publicKey, reception, amount, privateKey);
                if(payment.status === 200) {
                    this.props.setupProfile({ publicKey }, () => {
                        message.success('Payment Success');
                    });
                } else {
                    message.error("Errorrrrr");
                }
            } else {
                message.warning('Not enough energy');
            }
        } else {
            message.error('Must have a reception address');
        }
    }

    render() {
        const {reception, amount} = this.state;
        const suffixReception = reception ? <Icon type="close-circle" onClick={this.handleEmitEmptyReception} /> : null;
        return (
            <Card>
                <h3><Icon type="bank" /> Transfer Money</h3>
                <Row>
                    <Input
                        style={{width: 400}}
                        placeholder="Reception address ..."
                        prefix={<Icon type="key" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        suffix={suffixReception}
                        value={reception}
                        onChange={this.handleReceptionChange}
                        ref={node => this.addressInput = node}
                    />
                </Row>
                <Row style={{marginTop: 5}}>
                    <InputNumber
                        style={{width: 400}}
                        min={1}
                        value={amount}
                        onChange={this.handleAmountChange}
                    />
                </Row>
                <Row style={{marginTop: 5}}>
                    <TweetButton type="primary" style={{width: 200}}
                        clicked={this.handleTransfer}
                    >
                        Transfer Money
                    </TweetButton>
                </Row>
            </Card>
        );
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    profile: state.profile,
});

export default connect(mapStateToProps, {
    setupProfile,
    sendRequest
})(TransferMoney);